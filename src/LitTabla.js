import{html,css,LitElement} from'lit-element';
import'./LitDatos.js'

export class LitTabla extends LitElement{
    constructor(){
        super();
        this.menu=[];
        this.addEventListener('ApiData',(e)=>{
          this.__dataFormat(e.detail.data.results);
        })
    }
    static get properties(){
      return{
          menu:{type:String}
      };
  }
    __dataFormat(data){
let datos=[];
let stations=[];
let indexes=[];
let measurements=[];
data.forEach(s=>{
  s.stations.forEach(st=>{
    stations.push({
      id:st.id,
      name:st.name,
    })
  })
});

data.forEach(st=>{
  st.stations.forEach(i=>{
    indexes.push({
      calculationTime:i.calculationTime,
      responsiblePollumant:i.responsiblePollumant,
      value:i.value,
      scale: i.scale
    })
  })
});

data.forEach(s=>{
  s.stations.forEach(st=>{
    st.measurements.forEach(m=>{
      measurements.push({
        averagedOverInHours:m.averagedOverInHours,
        time:m.time,
        value2:m.value2,
        unit:m.unit,
        pollutant:m.pollutant
      })
    })
  })
});

  this.menu=stations;
  console.log(this.menu)
    }



    get dataTable(){
        return html`
         <style>
            thead{
                background-color: black;
                color: white;
            }
            td{
                background-color: rgb(167,167,167);
                color:rgb(54,54,227);
            }
            table,tbody,td{
                border:1px solid black;
            }
        </style>
          <table>
    <thead>
      <tr>
        <th>name</th>
        <th>id</th>
        <th>calculationTime</th>
        <th>responsiblePollumant</th>
        <th>Valor</th>
        <th>averagedOverInHours</th>
        <th>time</th>
        <th>unidad</th>
        <th>pollutant</th>
      </tr>
    </thead>
    <tbody>
    ${this.menu.map(datos=> html`
          <tr>
            <td>${datos.name}</td>
            <td>${datos.id}</td>
            <td>${datos.calculationTime}</td>
            <td>${datos.responsiblePollumant}</td>
            <td>${datos.value}</td>
            <td>${datos.averagedOverInHours}</td>
            <td>${datos.time}</td>
            <td>${datos.unit}</td>
            <td>${datos.pollutant}</td>
          </tr>`)}
    </tbody>
  </table>`
    }

    render(){
      return html`
      <lit-datos url="https://api.datos.gob.mx/v1/calidadAire" method="GET"></lit-datos>
      ${this.dataTable}
      `
    }
}