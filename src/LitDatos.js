import{html,css,LitElement} from'lit-element';
import'../lit-examen.js'

export class LitDatos extends LitElement{
    static get properties(){
        return{
            url:{type:String},
            method:{type:String}
        };
    }
//api: https://api.datos.gob.mx/v1/calidadAire
    __sendDatos(data){
        this.dispatchEvent(new CustomEvent('ApiData',{
            detail:{data},bubbles: true, composed: true
        }));
    }

    getDatos(){
        fetch(this.url,{method: this.method})
        .then((response)=>{
            if(response.ok)return response.json();
            return Promise.reject(response);
        })
        .then((data)=> {this.__sendDatos(data);})
        .catch((error)=>{console.warn('algo salio mal',error)})
    }

    firstUpdated(){
        this.getDatos();
    }

constructor(){
    super();
    this.getDatos;
}


}